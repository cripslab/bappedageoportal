<?php
error_reporting (E_ALL ^ E_NOTICE);
ob_start();
$param = $_GET['p'];

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Geoportal Makassar</title>

<?php include "get-css.php"; ?>

</head>

<body class="hold-transition">
    
    <!-- Bagian Header -->
    <section id="top"><?php include "applications/navigasi/header.php"; ?></section>
    
    <!-- Bagian Konten -->
    <section id="content"><?php include "applications/controller/controller.php" ?></section>

    <!-- Bagian Footer -->
    <section id="bottom"><?php include "applications/navigasi/footer.php"; ?></section>

<?php include "get-js.php"; ?>

</body>

</html>