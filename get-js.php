<!-- Js -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="dist/bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/owl-carousel/owl.carousel.js"></script>
<script src="plugins/carousel/jquery.carouFredSel-6.2.1.js"></script>
<script src="dist/js/weather.js"></script>
<script src="dist/js/app.js"></script>

<!-- Api    -->
<script src="dist/js/app-giscloud.js"></script>
