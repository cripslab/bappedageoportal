<div class="slide-full">
    <div id="owl-slide-full" class="owl-carousel owl-theme">
        <div class="item" style="background: url('dist/img/image/slider-utama/portofmakassar.jpg');">
            <div class="caption">
                <h2>Sistem Informasi Geoportal Kota Makassar</h2>
                <p>Mari Bersama Memantau Pembangunan Kota Kita</p>
                <a href="" class="btn btn-warning"><i class="fa fa-map"></i> Lihat GIS</a>
            </div>
        </div>
        <div class="item" style="background: url('dist/img/image/slider-utama/anjungan-losari.jpg');">
            <div class="caption">
                <h2>Berkontribusi Dengan e-Pelaporan</h2>
                <p>Ikut Aktif Mengawasi Pembangunan Kota Kita</p>
                <a href="" class="btn btn-success"><i class="fa fa-bullhorn"></i> Partisipasi</a>
            </div>
        </div>
    </div>
</div>

<div class="box-geoinfo">
    <div class="container">
        <div class="box-fitur">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="header-fitur">
                            <h3>Fitur Pemetaan</h3>
                            <p>Gunakan Fitur Untuk Melihat Peta Berdasar Jenisnya</p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <ul class="list-fitur" id="lisfit">
                            <li class="item-fitur">
                                <a href="index.php?p=geo-gis-daerah" target="_blank">
                                    <span class="icon-item-fitur"><i class="fa fa-map-marker"></i></span>
                                    <span class="text-item-fitur">Infrastruktur</span>
                                </a>
                            </li>
                            <li class="item-fitur">
                                <a href="">
                                    <span class="icon-item-fitur"><i class="fa fa-map-marker"></i></span>
                                    <span class="text-item-fitur">Kemiringan lereng</span>
                                </a>
                            </li>
                            <li class="item-fitur">
                                <a href="">
                                    <span class="icon-item-fitur"><i class="fa fa-map-marker"></i></span>
                                    <span class="text-item-fitur">Administrasi</span>
                                </a>
                            </li>
                            <li class="item-fitur">
                                <a href="">
                                    <span class="icon-item-fitur"><i class="fa fa-map-marker"></i></span>
                                    <span class="text-item-fitur">Geologi</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h4><i class="fa fa-map text-aqua"></i> Informasi <span class="text-aqua">Infrastruktur</span></h4></div>
                    <div class="box-body">
                        <div class="map-widget">
                            <iframe width="100%" height="100%" frameborder="0" src="https://mapsengine.google.com/map/u/0/embed?mid=z8IhMi3HVN6o.k3-sVaD4eX_o" marginwidth="0" marginheight="0" scrolling="no"></iframe>
                        </div>
                    </div>
                    <div class="box-footer"><a href=""><i class="fa fa-share-square-o pull-right text-aqua"></i></a></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h4><i class="fa fa-bullhorn text-green"></i> Laporan <span class="text-green">Masyarakat</span></h4></div>
                    <div class="box-body">
                        <div class="list-text">
                            <div class="item">
                                <div class="avatar"><img src="dist/img/theme/avatar.jpg"></div>
                                <div class="text">
                                    <h2 class="sender"><a href="">Lukman hafid</a></h2>
                                    <h2 class="title"><a href="index.php?p=geo-detail-report">E-KTP Berstatus Ganda Karena Pindah Domisili</a></h2>
                                    <p>Saya mau bertanya tentang E-KTP. Saya pernah melakukan perekaman E-KTP di daerah NTB dan sudah...</p>
                                    <div class="summary">
                                        <span class="date"><a href=""><span class="glyphicon glyphicon-time"></span> 2 jam lalu</a>
                                        </span>
                                        <span class="type"><a href=""><span class="glyphicon glyphicon-ok-circle"></span> laporan</a>
                                        </span>
                                        <span class="respon"><a href=""><span class="glyphicon glyphicon-ok"></span> 1 Tanggapan</a>
                                        </span>
                                        <span class="respon"><a href=""><span class="glyphicon glyphicon-thumbs-up"></span> Lihat peta</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="avatar"><img src="dist/img/theme/avatar.jpg"></div>
                                <div class="text">
                                    <h2 class="sender"><a href="">Ratna</a></h2>
                                    <h2 class="title"><a href="detail.html">Nasib Pengangkatan Guru CPNS Tenaga Honorer</a></h2>
                                    <p>Yth Kementerian Pendayagunaan Aparatur Negara dan Reformasi Birokrasi, Saya mendengar Menteri...</p>
                                    <div class="summary">
                                        <span class="date"><a href=""><span class="glyphicon glyphicon-time"></span> 2 jam lalu</a>
                                        </span>
                                        <span class="type"><a href=""><span class="glyphicon glyphicon-ok-circle"></span> Laporan</a>
                                        </span>
                                        <span class="respon"><a href=""><span class="glyphicon glyphicon-ok"></span> 1 Tanggapan</a>
                                        </span>
                                        <span class="respon"><a href=""><span class="glyphicon glyphicon-thumbs-up"></span> Lihat Peta</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer"><a href="index.php?p=geo-list-report" target="_blank"><i class="fa fa-share-square-o pull-right text-green"></i></a></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="box-content">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="box box-warning">
                    <div class="box-header">
                        <h4><i class="fa fa-chevron-right small"></i> Berita Terkini</h4>
                    </div>
                    <div class="box-body">
                    <div class="box-content-news">
                        <div class="side-news">
                            <div class="side-news-left">
                                <ul class="news-box">
                                    <li class="item-news">
                                        <h2 class="title"><a href="index.php?p=detail-news" target="_blank">Samsat Makassar Buka Gerai Baru di Bukit Katulistiwa</a></h2>
                                        <div class="detail">
                                            <span class="date">senin, 1/30/2014</span>
                                            <span class="author">Admin</span>
                                        </div>
                                        <div class="text">
                                            <div class="img-news">
                                                <img src="dist/img/image/foto/people%20(7).jpg">
                                            </div>
                                            <p>Samsat Makassar kini memiliki kantor pelayanan di Daya Makassar. Kantor pembantu pelayanan samsat Makassar ini terletak di Ruko Bukit Katulistiwa Jl perintis Kemerdekaan (Daya) Makassar. Rencananya akan diresmikan 7 Agustus 2014 mendatang. "Saya harapkan layanan ini dapat mempermudah dan tidak merepotkan lagi masyarakat Daya...</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="side-news-right">
                                <ul class="list-before">
                                    <h3>Berita Sebelumnya</h3>
                                    <li><a href="">but also the leap into electronic typesetting</a>
                                    </li>
                                    <li><a href="">for 'lorem ipsum' will uncover</a>
                                    </li>
                                    <li><a href="">of classical Latin literature from 45 BC</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="box-footer">
                        <div class="pagi-center">
                            <ul class="pagination pagination-sm">
                                <li class="disabled"><a href="#">&laquo;</a>
                                </li>
                                <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a>
                                </li>
                                <li><a href="">2</a>
                                </li>
                                <li><a href="">3</a>
                                </li>
                                <li><a href="">4</a>
                                </li>
                                <li><a href="">5</a>
                                </li>
                                <li><a href="#">&raquo;</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="box">
                    <img class="ads-img-long" src="dist/img/image/ads/long/ads-long-wellcome.gif">
                </div>

                <div class="row">
                    <div class="col-md-9">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h4><i class="fa fa-bullhorn text-aqua"></i> Jawaban <span class="text-aqua">Pemerintah</span></h4>
                            </div>                            
                            <div class="box-body">
                                <div class="list-text">
                                    <div class="item">
                                        <div class="avatar"><img src="dist/img/theme/avatar.jpg"></div>
                                        <div class="text">
                                            <h2 class="sender"><a href="">Dinas Pekerjaan Umum</a></h2>
                                            <h2 class="title"><a href="detail.html">E-KTP Berstatus Ganda Karena Pindah Domisili</a></h2>
                                            <p>Saya mau bertanya tentang E-KTP. Saya pernah melakukan perekaman E-KTP di daerah NTB dan sudah...</p>
                                            <div class="summary">
                                                <span class="date"><a href=""><span class="glyphicon glyphicon-time"></span> 2 jam lalu</a>
                                                </span>
                                                <span class="type"><a href=""><span class="glyphicon glyphicon-ok-circle"></span> laporan</a>
                                                </span>
                                                <span class="respon"><a href=""><span class="glyphicon glyphicon-ok"></span> 1 Tanggapan</a>
                                                </span>
                                                <span class="respon"><a href=""><span class="glyphicon glyphicon-thumbs-up"></span> Lihat peta</a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer"><a href=""><i class="fa fa-share-square-o pull-right text-aqua"></i></a></div>
                        </div>
                    </div>
                    <div class="col-md-3">                
                        <div class="box box-warning">
                            <img class="ads-img-short" src="dist/img/image/ads/short/ads-short-lapor.jpg">
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-4">
                <?php include "applications/navigasi/sidebar.php"; ?>
            </div>
        </div>
    </div>
</div>