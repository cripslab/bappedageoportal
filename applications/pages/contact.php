<div class="box-content">
    <?php include "applications/navigasi/sidetop.php"; ?>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="box box-success">
                        <div class="box-header">
                            <h4><i class="fa fa-chevron-right small"></i> Hubungi kami</h4>
                        </div>
                        <div class="box-body">
                            <div class="box-address-contact">
                                                           <blockquote>
                                <p class="lead text-green">Alamat Kantor</p>
                                <p>Badan Perencanaan Pembangunan Kota Makassar</p>
                            </blockquote>
                                <div class="text">
                                    <ul class="list-unstyled">
                                        <p>Untuk informasi lebih lanjut silahkan hubungi kami atau langsung datang ke kantor resmi Badan Perencanaan Pembangunan Kota Makassar, sesuai dengan kontak dibawah ini</p>
                                        <li><span>Alamat :</span> Jl. Ahmad Yani No.2, Ujung Pandang, Kota Makassar, Sulawesi Selatan 90111, Indonesia</li>
                                        <li><span>Email :</span> info@bappeda.makassar.go.id</li>
                                        <li><span>Nomor Telepon :</span> +62 411 316940</li>
                                        <li><span>Website :</span> bappeda.makassar.go.id</li>
                                    </ul>
                                </div>
                                <div class="map"></div>
                            </div>
                            <div class="box-message-contact">
                                <p>Atau, anda dapat menghubungi kami langsung dengan mengirimkan pesan melalui form dibawah ini</p>
                                <form role="form">
                                    <div class="item">
                                        <label for="">Nama Lengkap <span>:</span></label>
                                        <input type="text" class="form-control frm-contact" id="" placeholder="nama lengkap">
                                    </div>
                                    <div class="item">
                                        <label for="">Alamat EMail <span>:</span></label>
                                        <input type="email" class="form-control frm-contact" id="" placeholder="nama panggilan">
                                    </div>
                                    <div class="item">
                                        <label for="">Nomor Telepon <span>:</span></label>
                                        <input type="text" class="form-control frm-contact" id="" placeholder="nama lengkap">
                                    </div>
                                    <div class="item">
                                        <label for="">Kolom Komentar <span>:</span></label>
                                        <textarea class="form-control"></textarea>
                                    </div>
                                    <div class="itm-btn">
                                        <button type="button" class="btn btn-warning">Kirim</button>
                                        <button type="button" class="btn btn-default">Batal</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="box-footer">

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <?php include "applications/navigasi/sidebar.php"; ?>
                </div>
            </div>
        </div>
</div>