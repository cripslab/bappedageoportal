<div class="box-content">
    <?php include "applications/navigasi/sidetop.php"; ?>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="box box-success">
                        <div class="box-header">
                            <h4><i class="fa fa-chevron-right small"></i> Visi Dan Misi</h4>
                        </div>
                        <div class="box-body">
                            <blockquote>
                                <p class="lead text-aqua">Visi & Misi</p>
                                <p> Visi dan Misi Badan Perencanaan Pembangunan Kota Makassar</p>
                            </blockquote>
                            <hr>
                            <div class="content">
                                <h3 class="text-green">Visi :</h3>
                                <p class="quote">Tersedianya infrastruktur yang memadai, bermanfaat dan berkelanjutan berlandaskan falsafah Tri Hita Karana</p>

                                <h3 class="text-orange">Misi :</h3>
                                <ul class="quote">
                                    <li>Menyusun perencanaan infrastruktur yang terpadu dan berkelanjutan;</li>
                                    <li>Mengoptimalkan fungsi infrastruktur yang ada melalui pemeliharaan, peningkatan, pengawasan dan pengendalian;</li>
                                    <li>Meningkatkan dan mengembangkan pelaksanaan urusan pembangunan meliputi pekerjaan umum, perumahan, penataan ruang serta energi dan sumber daya mineral oleh pemerintah bersama swasta dan masyarakat;</li>
                                    <li>Melaksanakan organisasi yang efisien, efektif dan sinergis serta meningkatkan profesionalisme sumber daya manusia.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="box-footer">

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <?php include "applications/navigasi/sidebar.php"; ?>
                </div>
            </div>
        </div>
</div>