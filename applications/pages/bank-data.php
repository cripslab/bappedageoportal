<div class="box-content">
    <?php include "applications/navigasi/sidetop.php"; ?>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="box box-success">
                        <div class="box-header">
                            <h4><i class="fa fa-chevron-right small"></i> Visi Dan Misi</h4>
                        </div>
                        <div class="box-body">
                            <blockquote>
                                <p class="lead text-aqua">Visi & Misi</p>
                                <p> Visi dan Misi Badan Perencanaan Pembangunan Kota Makassar</p>
                            </blockquote>
                            <div class="box-table">

                                <table id="data-table" class="display table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%">#</th>
                                            <th>Bank Data Dinas Pekerjaan Umum</th>
                                            <th style="width: 5%"></th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td><a href="">1</a></td>
                                            <td><a href="">Many desktop publishing packages and web page editors now use Lorem Ipsum</a></td>
                                            <td><a href="" rel="tooltip" title="Download"><span class="glyphicon glyphicon-download"></span></a></td>
                                        </tr>
                                        <tr>
                                            <td><a href="">2</a></td>
                                            <td><a href="">There are many variations of passages of Lorem Ipsum available</a></td>
                                            <td><a href="" rel="tooltip" title="Download"><span class="glyphicon glyphicon-download"></span></a></td>
                                        </tr>
                                        <tr>
                                            <td><a href="">3</a></td>
                                            <td><a href="">Contrary to popular belief, Lorem Ipsum is not simply random text</a></td>
                                            <td><a href="" rel="tooltip" title="Download"><span class="glyphicon glyphicon-download"></span></a></td>
                                        </tr>
                                        <tr>
                                            <td><a href="">4</a></td>
                                            <td><a href="">The standard chunk of Lorem Ipsum used since the 1500s</a></td>
                                            <td><a href="" rel="tooltip" title="Download"><span class="glyphicon glyphicon-download"></span></a></td>
                                        </tr>
                                        <tr>
                                            <td><a href="">5</a></td>
                                            <td><a href="">Lorem Ipsum has been the industry's standard dummy text ever since</a></td>
                                            <td><a href="" rel="tooltip" title="Download"><span class="glyphicon glyphicon-download"></span></a></td>
                                        </tr>
                                        <tr>
                                            <td><a href="">6</a></td>
                                            <td><a href="">The generated Lorem Ipsum is therefore always free from repetition</a></td>
                                            <td><a href="" rel="tooltip" title="Download"><span class="glyphicon glyphicon-download"></span></a></td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <div class="box-footer">

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <?php include "applications/navigasi/sidebar.php"; ?>
                </div>
            </div>
        </div>
</div>