<script type="text/javascript" src="http://api.giscloud.com/1/api.js"></script>
 
   <div class="box-content-geo">
    <?php include "applications/navigasi/sidetop-geo.php"; ?>
        <div class="row">
            <div class="box-geo-full">
                <div id="sidebar-layer" class="col-md-3 scrollDiv pull-left" style="display: none;">
                    <h3><i class="fa fa-map"></i> Batasi Pencitraan</h3>
                    <div id="layerList"></div>
                </div>
                <div class="col-md-12" id="content-map">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left side-tools">
                            <div class="btn-group" role="group" aria-label="...">
                               <button type="button" onClick="" class="btn btn-success btn-sm" id="toggleSidebar"><i class="fa fa-map-o"></i></button>
                            </div>
                        </div>
                        <h4><i class="fa fa-chevron-right small"></i> GIS Profil Daerah</h4>
                    </div>
                    <div class="row">
                        <div class="col-md-6"><h3 class="title-gis-tools">&nbsp; &nbsp; Alat Standar :</h3> <div id="toolbar"></div></div>
                        <div class="col-md-5 bg-green hidden-sm"><h3 class="title-gis-tools">Alat Drawing :</h3> <div id="toolbar_2"></div></div>
                    </div>
                    <div id="mapViewer"></div>
                    
                </div>
                </div>
            </div>
        </div>
</div>