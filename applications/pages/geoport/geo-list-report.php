<div class="box-content-geo">
    <?php include "applications/navigasi/sidetop-geo.php"; ?>

            <div class="row">
                <div class="col-md-8 box-geo-center">
                    <div class="box box-info">
                        <div class="box-header">
                            <h4><i class="fa fa-chevron-right small"></i> List laporan</h4>
                            <div class="pull-right box-tools">
                                <div class="btn-group" role="group" aria-label="...">
                                    <button type="button" onClick="location.href='index.php?p=geo-list-report'" class="btn btn-info btn-sm"><i class="fa fa-list"></i></button>
                                    <button type="button" onClick="location.href='index.php?p=geo-map-report'" class="btn btn-success btn-sm"><i class="fa fa-map"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="list-text">
                                <div class="item">
                                    <div class="avatar"><img src="dist/img/theme/avatar.jpg"></div>
                                    <div class="text">
                                        <h2 class="sender"><a href="">wandy</a></h2>
                                        <h2 class="title"><a href="detail.html">E-KTP Berstatus Ganda Karena Pindah Domisili</a></h2>
                                        <p>Kepada Yth. Pemerintah Provinsi DKI Jakarta. Saya ingin melaporkan bahwa Lampu penyebrangan orang di depan halte busway pecenongan dan kantor kemendagri padam. mohon dinas terkait untuk menindaklanjutinya. Terima kasih...</p>
                                        <div class="summary">
                                            <span class="date"><a href=""><span class="glyphicon glyphicon-time"></span> 2 jam lalu</a>
                                            </span>
                                            <span class="type"><a href=""><span class="glyphicon glyphicon-ok-circle"></span> Laporan</a>
                                            </span>
                                            <span class="respon"><a href=""><span class="glyphicon glyphicon-ok"></span> 1 Tanggapan</a>
                                            </span>
                                            <span class="respon"><a href=""><span class="glyphicon glyphicon-thumbs-up"></span> Lihat Peta</a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="avatar"><img src="dist/img/theme/avatar.jpg"></div>
                                    <div class="text">
                                        <h2 class="sender"><a href="">Ratno Salim</a></h2>
                                        <h2 class="title"><a href="detail.html">Nasib Pengangkatan Guru CPNS Tenaga Honorer</a></h2>
                                        <p>Yth Kementerian Pendayagunaan Aparatur Negara dan Reformasi Birokrasi, Saya mendengar Menteri...</p>
                                        <div class="summary">
                                            <span class="date"><a href=""><span class="glyphicon glyphicon-time"></span> 2 jam lalu</a>
                                            </span>
                                            <span class="type"><a href=""><span class="glyphicon glyphicon-ok-circle"></span> Laporan</a>
                                            </span>
                                            <span class="respon"><a href=""><span class="glyphicon glyphicon-ok"></span> 1 Tanggapan</a>
                                            </span>
                                            <span class="respon"><a href=""><span class="glyphicon glyphicon-thumbs-up"></span> Lihat Peta</a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="avatar"><img src="dist/img/theme/avatar.jpg"></div>
                                    <div class="text">
                                        <h2 class="sender"><a href="">Ogy Lukman</a></h2>
                                        <h2 class="title"><a href="detail.html">E-KTP Berstatus Ganda Karena Pindah Domisili</a></h2>
                                        <p>Kepada Yth. Pemerintah Provinsi DKI Jakarta. Saya ingin melaporkan bahwa Lampu penyebrangan orang di depan halte busway pecenongan dan kantor kemendagri padam. mohon dinas terkait untuk menindaklanjutinya. Terima kasih...</p>
                                        <div class="summary">
                                            <span class="date"><a href=""><span class="glyphicon glyphicon-time"></span> 2 jam lalu</a>
                                            </span>
                                            <span class="type"><a href=""><span class="glyphicon glyphicon-ok-circle"></span> Laporan</a>
                                            </span>
                                            <span class="respon"><a href=""><span class="glyphicon glyphicon-ok"></span> 1 Tanggapan</a>
                                            </span>
                                            <span class="respon"><a href=""><span class="glyphicon glyphicon-thumbs-up"></span> Lihat Peta</a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="avatar"><img src="dist/img/theme/avatar.jpg"></div>
                                    <div class="text">
                                        <h2 class="sender"><a href="">Arham Anwar</a></h2>
                                        <h2 class="title"><a href="detail.html">E-KTP Berstatus Ganda Karena Pindah Domisili</a></h2>
                                        <p>Kepada Yth. Pemerintah Provinsi DKI Jakarta. Saya ingin melaporkan bahwa Lampu penyebrangan orang di depan halte busway pecenongan dan kantor kemendagri padam. mohon dinas terkait untuk menindaklanjutinya. Terima kasih...</p>
                                        <div class="summary">
                                            <span class="date"><a href=""><span class="glyphicon glyphicon-time"></span> 2 jam lalu</a>
                                            </span>
                                            <span class="type"><a href=""><span class="glyphicon glyphicon-ok-circle"></span> Laporan</a>
                                            </span>
                                            <span class="respon"><a href=""><span class="glyphicon glyphicon-ok"></span> 1 Tanggapan</a>
                                            </span>
                                            <span class="respon"><a href=""><span class="glyphicon glyphicon-thumbs-up"></span> Lihat Peta</a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="avatar"><img src="dist/img/theme/avatar.jpg"></div>
                                    <div class="text">
                                        <h2 class="sender"><a href="">M. Fahmi</a></h2>
                                        <h2 class="title"><a href="detail.html">E-KTP Berstatus Ganda Karena Pindah Domisili</a></h2>
                                        <p>Kepada Yth. Pemerintah Provinsi DKI Jakarta. Saya ingin melaporkan bahwa Lampu penyebrangan orang di depan halte busway pecenongan dan kantor kemendagri padam. mohon dinas terkait untuk menindaklanjutinya. Terima kasih...</p>
                                        <div class="summary">
                                            <span class="date"><a href=""><span class="glyphicon glyphicon-time"></span> 2 jam lalu</a>
                                            </span>
                                            <span class="type"><a href=""><span class="glyphicon glyphicon-ok-circle"></span> Laporan</a>
                                            </span>
                                            <span class="respon"><a href=""><span class="glyphicon glyphicon-ok"></span> 1 Tanggapan</a>
                                            </span>
                                            <span class="respon"><a href=""><span class="glyphicon glyphicon-thumbs-up"></span> Lihat Peta</a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <?php include "applications/navigasi/sidemenu-user-geo.php"; ?>
                </div>
            </div>

</div>