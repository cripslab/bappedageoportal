<div class="box-content-geo">
    <?php include "applications/navigasi/sidetop-geo.php"; ?>

            <div class="row">
                <div class="col-md-8 box-geo-center">
                   <div class="box box-success">
                        <div class="box-header">
                            <h4><i class="fa fa-chevron-right small"></i> Map laporan</h4>
                            <div class="pull-right box-tools">
                                <div class="btn-group" role="group" aria-label="...">
                                    <button type="button" onClick="location.href='index.php?p=geo-list-report'"  class="btn btn-info btn-sm"><i class="fa fa-list"></i></button>
                                    <button type="button" onClick="location.href='index.php?p=geo-map-report'" class="btn btn-success btn-sm"><i class="fa fa-map"></i></button>
                                </div>
                            </div>
                        </div>
                    <iframe width="100%" height="700px" frameborder="0" src="https://mapsengine.google.com/map/u/0/embed?mid=z8IhMi3HVN6o.k3-sVaD4eX_o" marginwidth="0" marginheight="0" scrolling="no"></iframe>
                </div>
                </div>
                <div class="col-md-4">
                    <?php include "applications/navigasi/sidemenu-user-geo.php"; ?>
                </div>
            </div>

</div>