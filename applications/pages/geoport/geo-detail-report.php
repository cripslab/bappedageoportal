<div class="box-content-geo">
    <?php include "applications/navigasi/sidetop-geo.php"; ?>

        <div class="row">
            <div class="col-md-8 box-geo-center">
                <div class="box box-warning">
                    <div class="box-header">
                        <h4><i class="fa fa-chevron-right small"></i> Detail laporan</h4>
                    </div>
                    <div class="box-body">
                        <div class="detail-report">
                            <div class="detail-report-text">
                                <h2 class="title">Nasib Pengangkatan Guru CPNS Tenaga Honorer</h2>
                                <h3 class="sub-head">Laporan :</h3>
                                <p>It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
                                <p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                                <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
                                <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                            </div>
                            <h3 class="sub-head">Lihat Lokasi : <a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" aria-expanded="true" class="" data-original-title="peta"><i class="fa fa-map"></i></a></h3>
                            <div id="accordion">
                                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                                <div class="panel">
                                    <div class="panel-collapse collapse" id="collapseOne" aria-expanded="true" style="">
                                        <div class="">
                                            <iframe width="100%" height="250" frameborder="0" src="https://mapsengine.google.com/map/u/0/embed?mid=z8IhMi3HVN6o.k3-sVaD4eX_o" marginwidth="0" marginheight="0" scrolling="no"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-9">
                                    <h3 class="sub-head">Foto :</h3>
                                    <div class="box-img-lapor">
                                        <div id="img-lapor">
                                            <div class="item"><img src="dist/img/image/img-konten/post-img/Jalan-Rusak.jpg"></div>
                                            <div class="item"><img src="dist/img/image/img-konten/post-img/Jalan-Rusak.jpg"></div>
                                        </div>
                                        <div class="img-lapor-tools">
                                            <a class="prev" id="foo2_prev" href="#"><i class="fa fa-caret-square-o-up text-green"></i></a>
	                                        <a class="next" id="foo2_next" href="#"><i class="fa fa-caret-square-o-down text-green"></i></a>
	                                    </div>
                                    </div>

                                    <!-- Nav tabs -->
                                    <ul class="nav tab-comment" role="tablist">
                                        <li class="active"><a href="#tindakan" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-bullhorn"></span> Tindak lanjut laporan</a></li>
                                        <li><a href="#komentar" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-comment"></span> Komentar Umum</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tindakan">
                                            <div class="box-message">
                                                <div class="box-disposisi">
                                                    <div class="text">Laporan ini di disposisikan ke Dinas :</div>
                                                    <div class="list-select">
                                                        <form action="">
                                                            <div class="input-group">
                                                                <select class="form-control">
                                                                    <option>Dinas Komunikasi Dan Informasi</option>
                                                                    <option>Dinas Kesehatan</option>
                                                                    <option>Dinas Pekerjaan Umum</option>
                                                                    <option>Dinas Pendidikan</option>
                                                                    <option>Dinas Kehutanan</option>
                                                                </select>
                                                                <span class="input-group-btn">
                                                            <button class="btn btn-success" type="button">
                                                                <span class="glyphicon glyphicon-bullhorn"></span>
                                                                </button>
                                                                </span>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div class="actionBox">
                                                    <ul class="commentList">
                                                        <li>
                                                            <div class="commenterImage">
                                                                <img src="dist/img/theme/avatar.jpg" />
                                                            </div>
                                                            <div class="commentText">
                                                                <p class="user"><a href="">Pemkot Makassar</a></p>
                                                                <p class="comment">Didisposisikan ke <a href="">Dinas Kominfo</a> (Pemerintah Kota Makassar) Salinan ke Kantor Walikota Makassar (Pemerintah Kota Makassar) </p>
                                                                <span class="date sub-text">on March 5th, 2014</span>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="commenterImage">
                                                                <img src="dist/img/theme/avatar.jpg" />
                                                            </div>
                                                            <div class="commentText">
                                                                <p class="user"><a href="">Dinas Kominfo</a></p>
                                                                <p class="comment">Terimakasih atas partisipasi anda, kami akan segera menindak lanjuti laporan anda.</p>
                                                                <span class="date sub-text">on March 5th, 2014</span>

                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="commentBox">
                                                    <form class="form-inline" role="form">
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" placeholder="Your comments" />
                                                        </div>
                                                        <div class="form-group">
                                                            <button class="btn btn-default">Komentar</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="komentar">
                                            <div class="box-message">
                                                <div class="titleBox">
                                                    <label>Kolom komentar</label>
                                                    <button type="button" class="close" aria-hidden="true">&times;</button>
                                                </div>
                                                <div class="actionBox">
                                                    <ul class="commentList">
                                                        <li>
                                                            <div class="commenterImage">
                                                                <img src="dist/img/theme/avatar.jpg" />
                                                            </div>
                                                            <div class="commentText">
                                                                <p class="user"><a href="">wandy rifaldi</a></p>
                                                                <p class="comment">Hello this is a test comment.</p>
                                                                <span class="date sub-text">on March 5th, 2014</span>

                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="commenterImage">
                                                                <img src="dist/img/theme/avatar.jpg" />
                                                            </div>
                                                            <div class="commentText">
                                                                <p class="user"><a href="">arham anwar</a></p>
                                                                <p class="comment">Hello this is a test comment and this comment is particularly very long and it goes on and on and on.</p>
                                                                <span class="date sub-text">on March 5th, 2014</span>

                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="commenterImage">
                                                                <img src="dist/img/theme/avatar.jpg" />
                                                            </div>
                                                            <div class="commentText">
                                                                <p class="user"><a href="">wahdan</a></p>
                                                                <p class="comment">Hello this is a test comment.</p>
                                                                <span class="date sub-text">on March 5th, 2014</span>

                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="commentBox">
                                                    <form class="form-inline" role="form">
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" placeholder="Your comments" />
                                                        </div>
                                                        <div class="form-group">
                                                            <button class="btn btn-default">komentar</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <h3 class="sub-head">Data Laporan :</h3>
                                    <div class="detail-summary">
                                        <p>TRACKING ID#: <span>1243820</span></p>
                                        <p>USER: <span>Anonim</span></p>
                                        <p>PLATFORM: <span class="label label-danger">SMS</span></p>
                                        <p>TANGGAL: <span>16 July 2014 08:30:54</span></p>
                                        <p>KATEGORI: <span class="label label-danger">Infrastruktur</span></p>
                                        <p>AREA: <span class="label label-danger">Tamalanrea</span></p>
                                        <p>DILIHAT: <span>10</span></p>
                                        <p>DUKUNGAN: <span>0</span></p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <?php include "applications/navigasi/sidemenu-user-geo.php"; ?>
            </div>
        </div>

</div>