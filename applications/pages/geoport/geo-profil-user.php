<div class="box-content-geo">
    <?php include "applications/navigasi/sidetop-geo.php"; ?>

        <div class="row">
            <div class="col-md-8 box-geo-center">
                <div class="box box-info">
                    <div class="box-header">
                        <h4><i class="fa fa-chevron-right small"></i> Profil Pengguna</h4>
                    </div>
                    <div class="box-body">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_userprofil" data-toggle="tab"><i class="fa fa-user"></i> Data Profil</a></li>
                                <li><a href="#tab_userpassword" data-toggle="tab"><i class="fa fa-unlock-alt"></i> Password</a></li>
                                <li><a href="#tab_usernotif" data-toggle="tab"><i class="fa fa-comments"></i> Notifikasi</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_userprofil">
                                    <form action="" method="">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="lead text-aqua"><i class="fa fa-bullhorn"></i> Informasi Data Pengguna</p>
                                                <p>Pastikan data yang tampil adalah data yang benar, sesuai dengan KTP atau identitas lainnya</p>
                                                <hr>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="avatar-user"><img src="dist/img/theme/avatar.jpg"></div>
                                                <div class="change-avatar-user bg-green">
                                                    <div class="form-group">
                                                        <input type="file" class="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label>Username :</label>
                                                    <div class="input-group col-md-6">
                                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                        <input type="text" class="form-control" placeholder="Username">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Nama Lengkap :</label>
                                                    <div class="input-group col-md-6">
                                                        <span class="input-group-addon"><i class="fa fa-check-square-o"></i></span>
                                                        <input type="text" class="form-control" placeholder="Nama lengkap">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Alamat :</label>
                                                    <div class="input-group col-md-6">
                                                        <span class="input-group-addon"><i class="fa fa-home"></i></span>
                                                        <input type="text" class="form-control" placeholder="Alamat">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Telepon :</label>
                                                    <div class="input-group col-md-6">
                                                        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                        <input type="text" class="form-control" placeholder="Nomor Telepon">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Jenis Kemalin :</label>
                                                    <div class="input-group col-md-4">
                                                        <span class="input-group-addon"><i class="fa fa-venus-mars"></i></span>
                                                        <select class="form-control" placeholder="">
                                                            <option></option>
                                                            <option>Laki-laki</option>
                                                            <option>Perempuan</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <button class="btn btn-info">Update</button>
                                    </form>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_userpassword">
                                    <form action="" method="">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="lead text-green"><i class="fa fa-unlock-alt"></i> Password Pengguna</p>
                                                <p>Password bersifat rahasia, ganti password anda secara berkala, agar tetap aman.</p>
                                                <hr>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label>Password Lama :</label>
                                                    <div class="input-group col-md-6">
                                                        <span class="input-group-addon"><i class="fa fa-unlock-alt"></i></span>
                                                        <input type="text" class="form-control" placeholder="Password lama">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Password Baru :</label>
                                                    <div class="input-group col-md-6">
                                                        <span class="input-group-addon"><i class="fa fa-pencil-square-o"></i></span>
                                                        <input type="text" class="form-control" placeholder="Password Baru">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Ulangi Password Baru :</label>
                                                    <div class="input-group col-md-6">
                                                        <span class="input-group-addon"><i class="fa fa-pencil-square-o"></i></span>
                                                        <input type="text" class="form-control" placeholder="Ulangi Password Baru">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <button class="btn btn-success">Ubah Password</button>
                                    </form>

                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_usernotif">
                                    <div class="box-list-notif">
                                        <ul class="notif-list">
                                            <li>
                                                <div class="notif-image">
                                                    <img src="dist/img/theme/avatar.jpg" />
                                                </div>
                                                <div class="notif-text">
                                                    <p><a href="">Dinas Pekerjaan Umum</a> menanggapi kembali laporan <a href="">#1092834</a> yang telah anda tanggapi</p>
                                                    <span class="date sub-text">Senin, 12 agustus 2014</span>

                                                </div>
                                            </li>
                                            <li>
                                                <div class="notif-image">
                                                    <img src="dist/img/theme/avatar.jpg" />
                                                </div>
                                                <div class="notif-text">
                                                    <p><a href="">Dinas Pekerjaan Umum</a> menanggapi laporan <a href="">#1092834</a> yang anda laporkan</p>
                                                    <span class="date sub-text">Senin, 12 agustus 2014</span>

                                                </div>
                                            </li>
                                            <li>
                                                <div class="notif-image">
                                                    <img src="dist/img/theme/avatar.jpg" />
                                                </div>
                                                <div class="notif-text">
                                                    <p><a href="">Pemerintah Kota</a> mendisposisikan laporan <a href="">#1092834</a> ke Dinas Pekerjaan Umum</p>
                                                    <span class="date sub-text">Senin, 12 agustus 2014</span>

                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- nav-tabs-custom -->
                    </div>
                    <div class="box-footer">

                    </div>
                </div>

                <div class="box box-success">
                    <div class="box-header">
                        <h4><i class="fa fa-chevron-right small"></i> List laporan Anda</h4>
                    </div>
                    <div class="box-body">
                        <div class="list-text">
                            <div class="item">
                                <div class="avatar"><img src="dist/img/theme/avatar.jpg"></div>
                                <div class="text">
                                    <h2 class="sender"><a href="">wandy</a></h2>
                                    <h2 class="title"><a href="detail.html">E-KTP Berstatus Ganda Karena Pindah Domisili</a></h2>
                                    <p>Kepada Yth. Pemerintah Provinsi DKI Jakarta. Saya ingin melaporkan bahwa Lampu penyebrangan orang di depan halte busway pecenongan dan kantor kemendagri padam. mohon dinas terkait untuk menindaklanjutinya. Terima kasih...</p>
                                    <div class="summary">
                                        <span class="date"><a href=""><span class="glyphicon glyphicon-time"></span> 2 jam lalu</a>
                                        </span>
                                        <span class="type"><a href=""><span class="glyphicon glyphicon-ok-circle"></span> Laporan</a>
                                        </span>
                                        <span class="respon"><a href=""><span class="glyphicon glyphicon-ok"></span> 1 Tanggapan</a>
                                        </span>
                                        <span class="respon"><a href=""><span class="glyphicon glyphicon-thumbs-up"></span> Lihat Peta</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="avatar"><img src="dist/img/theme/avatar.jpg"></div>
                                <div class="text">
                                    <h2 class="sender"><a href="">wandy</a></h2>
                                    <h2 class="title"><a href="detail.html">E-KTP Berstatus Ganda Karena Pindah Domisili</a></h2>
                                    <p>Kepada Yth. Pemerintah Provinsi DKI Jakarta. Saya ingin melaporkan bahwa Lampu penyebrangan orang di depan halte busway pecenongan dan kantor kemendagri padam. mohon dinas terkait untuk menindaklanjutinya. Terima kasih...</p>
                                    <div class="summary">
                                        <span class="date"><a href=""><span class="glyphicon glyphicon-time"></span> 2 jam lalu</a>
                                        </span>
                                        <span class="type"><a href=""><span class="glyphicon glyphicon-ok-circle"></span> Laporan</a>
                                        </span>
                                        <span class="respon"><a href=""><span class="glyphicon glyphicon-ok"></span> 1 Tanggapan</a>
                                        </span>
                                        <span class="respon"><a href=""><span class="glyphicon glyphicon-thumbs-up"></span> Lihat Peta</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="avatar"><img src="dist/img/theme/avatar.jpg"></div>
                                <div class="text">
                                    <h2 class="sender"><a href="">wandy</a></h2>
                                    <h2 class="title"><a href="detail.html">E-KTP Berstatus Ganda Karena Pindah Domisili</a></h2>
                                    <p>Kepada Yth. Pemerintah Provinsi DKI Jakarta. Saya ingin melaporkan bahwa Lampu penyebrangan orang di depan halte busway pecenongan dan kantor kemendagri padam. mohon dinas terkait untuk menindaklanjutinya. Terima kasih...</p>
                                    <div class="summary">
                                        <span class="date"><a href=""><span class="glyphicon glyphicon-time"></span> 2 jam lalu</a>
                                        </span>
                                        <span class="type"><a href=""><span class="glyphicon glyphicon-ok-circle"></span> Laporan</a>
                                        </span>
                                        <span class="respon"><a href=""><span class="glyphicon glyphicon-ok"></span> 1 Tanggapan</a>
                                        </span>
                                        <span class="respon"><a href=""><span class="glyphicon glyphicon-thumbs-up"></span> Lihat Peta</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <?php include "applications/navigasi/sidemenu-user-geo.php"; ?>
            </div>
        </div>

</div>