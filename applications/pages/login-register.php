<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Geoportal Makassar</title>
    
<!-- Bootstrap Master CSS -->
<link href="../../dist/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Default Master CSS -->
<link href="../../dist/css/style.css" rel="stylesheet">
<link href="../../dist/css/crips.min.css" rel="stylesheet">
<link href="../../dist/css/skins/_all-skins.min.css" rel="stylesheet">

<!-- All Plugins CSS -->
<link href="../../plugins/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet">
<link href="../../plugins/ionicons-2.0.1/css/ionicons.min.css" rel="stylesheet">
<link href="../../plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="../../plugins/owl-carousel/owl.theme.css" rel="stylesheet">
<link href="../../plugins/datepicker/datepicker3.css" rel="stylesheet">
<link href="../../plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="../../plugins/select2/select2.min.css" rel="stylesheet">
<link href="../../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet">

</head>
<body>
<section id="login">
        <div class="box-login-regis">
        <!-- Nav tabs -->
<ul class="nav nav-tabs navtab-cont" role="tablist" id="myTab">
  <li class="active"><a href="#login-tab" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-lock"></span> Masuk</a></li>
  <li><a href="#register-tab" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-user"></span> Daftar Sekarang</a></li>
</ul>

<div class="tab-content">
    <div class="tab-pane active" id="login-tab">
        <div class="box-login">
            <div class="login-social">
                <h2>Login dengan menggunakan akun media sosial</h2>
                <ul>
                    <li><a href=""><img src="../../dist/img/theme/social-icon/fb-regis.png"></a></li>
                    <li><a href=""><img src="../../dist/img/theme/social-icon/twitter-regis.png"></a></li>
                </ul>
            </div>
            <form data-toggle="validator" role="form" id="logform" class="form-signin">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
                        <input type="email" class="form-control" name="email" placeholder="Masukkan Alamat email" required>
                    </div>
                    <span class="help-block with-errors"></span>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                        <input type="password" class="form-control" name="password" placeholder="Masukkan Password" required>
                    </div>
                    <span class="help-block with-errors"></span>
                </div>
                <div class="forgot"><a href=""><span class="glyphicon glyphicon-question-sign"></span> Lupa password</a></div>
                <label class="checkbox">
                    <input type="checkbox" value="remember-me">Ingat saya (Hanya jika ini komputer anda)
                </label>
                <button class="btn btn-lg btn-info btn-block" type="submit">Login</button>
            </form>
        </div>
    </div>
    <div class="tab-pane" id="register-tab">
        <div class="box-form-regis">
            <p class="sub-text">Atau silakan membuat ID baru Anda dengan mengisi formulir registrasi singkat di bawah ini:</p>
            <form data-toggle="validator" role="form" id="regform" class="form-signin">
                <div class="form-group">
                    <input type="email" class="form-control" id="inputEmail" placeholder="Email anda" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="inputUname" placeholder="Nama Pengguna" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="inputFname" placeholder="Nama Lengkap" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="inputAddress" placeholder="Alamat Pengguna" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="inputPhone" placeholder="Nomor Telepon" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="inputPass" placeholder="Password anda" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="inputConfirmPass" data-match="#inputPass" placeholder="Ulangi Password anda" required>
                    <div class="help-block with-errors"></div>
                </div>

                <label class="checkbox">
                    <input type="checkbox" value="rememberMe" required>Saya telah membaca dan menyetujui Terms of Use 
                </label>

                <button type="submit" class="btn btn-lg btn-success btn-block">Daftar Sekarang</button>
            </form>
        </div>
    </div>
</div>
        </div>
    </section>
<div id="wrapper_bg"><img src="../../dist/img/theme/bg-portal.jpg"></div>
<!-- Js -->
<script src="../../plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="../../dist/bootstrap/js/bootstrap.min.js"></script>
<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/owl-carousel/owl.carousel.js"></script>
<script src="../../plugins/carousel/jquery.carouFredSel-6.2.1.js"></script>
<script src="../../dist/js/weather.js"></script>
<script src="../../dist/js/app.js"></script>


</body>
</html>