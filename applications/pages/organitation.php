<div class="box-content">
    <?php include "applications/navigasi/sidetop.php"; ?>
        <div class="container">
            <div class="row">
                <div class="col-md-8 box-employes">
                    <div class="box box-warning">
                        <div class="box-header">
                            <h4><i class="fa fa-chevron-right small"></i> Struktur Organisasi</h4>
                        </div>
                        <div class="box-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab-kabadsekretaris" data-toggle="tab"><span class="glyphicon glyphicon-user"></span> Pimpinan</a>
                                </li>
                                <li><a href="#tab-pegawai" data-toggle="tab"><span class="glyphicon glyphicon-user"></span> Pegawai Negeri Sipil</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-kabadsekretaris">
                                    <div class="list-employe">
                                        <div class="item">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <blockquote>
                                                        <p class="lead text-orange">Kepala Badan</p>
                                                        <p>Badan Perencanaan Pembangunan Kota Makassar</p>
                                                    </blockquote>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="detail">
                                                        <dt>Nama :</dt>
                                                        <dd>Joko Widodo</dd>
                                                        <dt>NIP :</dt>
                                                        <dd>70012 4538 0092</dd>
                                                        <dt>Golongan :</dt>
                                                        <dd>IV.b</dd>
                                                        <dt>Pangkat :</dt>
                                                        <dd>Pembina TK.I</dd>
                                                        <dt>Jabatan :</dt>
                                                        <dd>Kepala Dinas</dd>
                                                        <dt>Tempat/Tanggal lahir :</dt>
                                                        <dd>Lasusua / 19-09-1980</dd>
                                                        <dt>Alamat :</dt>
                                                        <dd>Jl. Perintis kemerdekaan</dd>
                                                        <dt>Agama :</dt>
                                                        <dd>Islam</dd>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="avatar">
                                                        <img src="dist/img/theme/avatar.jpg">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <blockquote>
                                                        <p class="lead text-orange">Sekretaris SKPD</p>
                                                        <p>Badan Perencanaan Pembangunan Kota Makassar</p>
                                                    </blockquote>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="detail">
                                                        <dt>Nama :</dt>
                                                        <dd>Joko Widodo</dd>
                                                        <dt>NIP :</dt>
                                                        <dd>70012 4538 0092</dd>
                                                        <dt>Golongan :</dt>
                                                        <dd>IV.b</dd>
                                                        <dt>Pangkat :</dt>
                                                        <dd>Pembina TK.I</dd>
                                                        <dt>Jabatan :</dt>
                                                        <dd>Kepala Dinas</dd>
                                                        <dt>Tempat/Tanggal lahir :</dt>
                                                        <dd>Lasusua / 19-09-1980</dd>
                                                        <dt>Alamat :</dt>
                                                        <dd>Jl. Perintis kemerdekaan</dd>
                                                        <dt>Agama :</dt>
                                                        <dd>Islam</dd>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="avatar">
                                                        <img src="dist/img/theme/avatar.jpg">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-pegawai">
                                    <div class="list-employe">
                                        <div class="box-table-employes">
                                                    <blockquote>
                                                        <p class="lead text-orange">Data Pegawai Negeri Sipil</p>
                                                        <p>Badan Perencanaan Pembangunan Kota Makassar</p>
                                                    </blockquote>

                                            <table id="data-table" class="display table-striped table-bordered dataTable" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 2%">#</th>
                                                        <th>Nama</th>
                                                        <th style="width: 5%">NIP</th>
                                                        <th style="width: 5%">Gol.</th>
                                                        <th style="width: 10%">Pangkat</th>
                                                        <th>Jabatan</th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <tr>
                                                        <td><a href="">1</a></td>
                                                        <td><a href="">wandy rifaldi</a></td>
                                                        <td><a href="">12345</a></td>
                                                        <td><a href="">IV.B</a></td>
                                                        <td><a href="">Pembina</a></td>
                                                        <td><a href="">Staf Seksi Verifikasi dan Pengawasan</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="">1</a></td>
                                                        <td><a href="">wandy rifaldi</a></td>
                                                        <td><a href="">12345</a></td>
                                                        <td><a href="">IV.B</a></td>
                                                        <td><a href="">Pembina</a></td>
                                                        <td><a href="">Staf Seksi Verifikasi dan Pengawasan</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="">1</a></td>
                                                        <td><a href="">wandy rifaldi</a></td>
                                                        <td><a href="">12345</a></td>
                                                        <td><a href="">IV.B</a></td>
                                                        <td><a href="">Pembina</a></td>
                                                        <td><a href="">Staf Seksi Verifikasi dan Pengawasan</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="">1</a></td>
                                                        <td><a href="">wandy rifaldi</a></td>
                                                        <td><a href="">12345</a></td>
                                                        <td><a href="">IV.B</a></td>
                                                        <td><a href="">Pembina</a></td>
                                                        <td><a href="">Staf Seksi Verifikasi dan Pengawasan</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <?php include "applications/navigasi/sidebar.php"; ?>
                </div>
            </div>
        </div>
</div>