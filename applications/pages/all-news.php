<div class="box-content">
    <?php include "applications/navigasi/sidetop.php"; ?>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="box box-warning">
                        <div class="box-header">
                            <h4><i class="fa fa-chevron-right small"></i> Semua Berita</h4>
                        </div>
                        <div class="box-body">
                            <ul class="news-box">
                                <li class="item-news">
                                    <h2 class="title"><a href="index.php?p=detail-news" target="_blank">Samsat Makassar Buka Gerai Baru di Bukit Katulistiwa</a></h2>
                                    <div class="detail">
                                        <span class="date">senin, 1/30/2014</span>
                                        <span class="author">Admin</span>
                                    </div>
                                    <div class="text">
                                        <div class="img-news">
                                            <img src="dist/img/image/foto/people%20(7).jpg">
                                        </div>
                                        <p>Samsat Makassar kini memiliki kantor pelayanan di Daya Makassar. Kantor pembantu pelayanan samsat Makassar ini terletak di Ruko Bukit Katulistiwa Jl perintis Kemerdekaan (Daya) Makassar. Rencananya akan diresmikan 7 Agustus 2014 mendatang. "Saya harapkan layanan ini dapat mempermudah dan tidak merepotkan lagi masyarakat Daya...</p>
                                    </div>
                                </li>
                                <li class="item-news">
                                    <h2 class="title"><a href="index.php?p=detail-news" target="_blank">Samsat Makassar Buka Gerai Baru di Bukit Katulistiwa</a></h2>
                                    <div class="detail">
                                        <span class="date">senin, 1/30/2014</span>
                                        <span class="author">Admin</span>
                                    </div>
                                    <div class="text">
                                        <div class="img-news">
                                            <img src="dist/img/image/foto/people%20(7).jpg">
                                        </div>
                                        <p>Samsat Makassar kini memiliki kantor pelayanan di Daya Makassar. Kantor pembantu pelayanan samsat Makassar ini terletak di Ruko Bukit Katulistiwa Jl perintis Kemerdekaan (Daya) Makassar. Rencananya akan diresmikan 7 Agustus 2014 mendatang. "Saya harapkan layanan ini dapat mempermudah dan tidak merepotkan lagi masyarakat Daya...</p>
                                    </div>
                                </li>
                                <li class="item-news">
                                    <h2 class="title"><a href="index.php?p=detail-news" target="_blank">Samsat Makassar Buka Gerai Baru di Bukit Katulistiwa</a></h2>
                                    <div class="detail">
                                        <span class="date">senin, 1/30/2014</span>
                                        <span class="author">Admin</span>
                                    </div>
                                    <div class="text">
                                        <div class="img-news">
                                            <img src="dist/img/image/foto/people%20(7).jpg">
                                        </div>
                                        <p>Samsat Makassar kini memiliki kantor pelayanan di Daya Makassar. Kantor pembantu pelayanan samsat Makassar ini terletak di Ruko Bukit Katulistiwa Jl perintis Kemerdekaan (Daya) Makassar. Rencananya akan diresmikan 7 Agustus 2014 mendatang. "Saya harapkan layanan ini dapat mempermudah dan tidak merepotkan lagi masyarakat Daya...</p>
                                    </div>
                                </li>
                                <li class="item-news">
                                    <h2 class="title"><a href="index.php?p=detail-news" target="_blank">Samsat Makassar Buka Gerai Baru di Bukit Katulistiwa</a></h2>
                                    <div class="detail">
                                        <span class="date">senin, 1/30/2014</span>
                                        <span class="author">Admin</span>
                                    </div>
                                    <div class="text">
                                        <div class="img-news">
                                            <img src="dist/img/image/foto/people%20(7).jpg">
                                        </div>
                                        <p>Samsat Makassar kini memiliki kantor pelayanan di Daya Makassar. Kantor pembantu pelayanan samsat Makassar ini terletak di Ruko Bukit Katulistiwa Jl perintis Kemerdekaan (Daya) Makassar. Rencananya akan diresmikan 7 Agustus 2014 mendatang. "Saya harapkan layanan ini dapat mempermudah dan tidak merepotkan lagi masyarakat Daya...</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="box-footer">
                            <div class="pagi-center">
                                <ul class="pagination pagination-sm">
                                    <li class="disabled"><a href="#">&laquo;</a>
                                    </li>
                                    <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a>
                                    </li>
                                    <li><a href="">2</a>
                                    </li>
                                    <li><a href="">3</a>
                                    </li>
                                    <li><a href="">4</a>
                                    </li>
                                    <li><a href="">5</a>
                                    </li>
                                    <li><a href="#">&raquo;</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <?php include "applications/navigasi/sidebar.php"; ?>
                </div>
            </div>
        </div>
</div>