<div class="header">
    <nav class="navbar navbar-geo">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-general" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <i class="fa fa-list"></i>
                        </button>
                        <a class="navbar-brand" href="index.php"><img src="dist/img/theme/logo-geoportal.jpg"></a>
                    </div>
                </div>
                <div class="col-md-9 col-sm-12">
                    <div class="collapse navbar-collapse" id="nav-general">
                        <div class="col-md-8 pull-right">
                            <ul class="nav-top navbar-right">
                                <li><a href="applications/pages/login-register.php" class="btn btn-warning btn-sm pull-right"><i class="fa fa-user"></i> Login/Daftar</a></li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="index.php"><i class="fa fa-home"></i></a></li>
                                <li>
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Informasi</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Selayang Pandang</a></li>
                                        <li><a href="">Profil Pimpinan</a></li>
                                        <li><a href="index.php?p=organitation">Struktur Organisasi</a></li>
                                        <li><a href="index.php?p=visi-misi">Visi & Misi</a></li>
                                    </ul>
                                </li>
                                <li><a href="index.php?p=geo-list-report">eLaporan</a></li>
                                <li><a href="index.php?p=geo-gis-daerah">Map Gis</a></li>
                                <li><a href="index.php?p=all-news">Berita</a></li>
                                <li><a href="index.php?p=bank-data">Bank Data</a></li>
                                <li><a href="index.php?p=contact">Kontak</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>