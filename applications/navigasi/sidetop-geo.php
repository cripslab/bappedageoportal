<div class="box-header-bg-geo" style="">

        <div class="box-fitur-full">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="header-fitur">
                            <h3>Fitur Pemetaan</h3>
                            <p>Gunakan Fitur Untuk Melihat Peta Berdasar Jenisnya</p>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <ul class="list-fitur" id="lisfit-geo">
                            <li class="item-fitur">
                                <a href="index.php?p=geo-gis-daerah" target="_blank">
                                    <span class="icon-item-fitur"><i class="fa fa-map-marker"></i></span>
                                    <span class="text-item-fitur">Infrastruktur</span>
                                </a>
                            </li>
                            <li class="item-fitur">
                                <a href="">
                                    <span class="icon-item-fitur"><i class="fa fa-map-marker"></i></span>
                                    <span class="text-item-fitur">Kemiringan lereng</span>
                                </a>
                            </li>
                            <li class="item-fitur">
                                <a href="">
                                    <span class="icon-item-fitur"><i class="fa fa-map-marker"></i></span>
                                    <span class="text-item-fitur">Administrasi</span>
                                </a>
                            </li>
                            <li class="item-fitur">
                                <a href="">
                                    <span class="icon-item-fitur"><i class="fa fa-map-marker"></i></span>
                                    <span class="text-item-fitur">Geologi</span>
                                </a>
                            </li>
                            <li class="item-fitur">
                                <a href="">
                                    <span class="icon-item-fitur"><i class="fa fa-map-marker"></i></span>
                                    <span class="text-item-fitur">Geologi</span>
                                </a>
                            </li>
                            <li class="item-fitur">
                                <a href="">
                                    <span class="icon-item-fitur"><i class="fa fa-map-marker"></i></span>
                                    <span class="text-item-fitur">Geologi</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

</div>