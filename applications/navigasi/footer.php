<div class="box-menu-foot">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="menu-foot">
                            <li><a href="index.php">Beranda</a></li>
                            <li><a href="">Berita</a></li>
                            <li><a href="">Galeri</a></li>
                            <li><a href="">Peta Situs</a></li>
                            <li><a href="">Tanya Jawab</a></li>
                            <li><a href="">Peraturan Pemerintah</a></li>
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <p class="copyright">Copyright <a href="http://www.cripxel.com" target="_blank">©</a> 2015 Badan Perencanaan Pembangunan Kota Makassar</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="content-foot">
                    <span>Pengunjung:</span>
                    <h2><i class="fa fa-heartbeat"></i> 10,054,060</h2>
                </div>
            </div>
        </div>
    </div>
</div>
