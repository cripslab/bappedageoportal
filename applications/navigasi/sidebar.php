<div class="box box-info bg-aqua">
    <div id="weather"></div>
</div>
<div class="box box-success">
    <div class="box-header">
        <h4><i class="fa fa-chevron-right small"></i> Data Kecamatan</h4>
    </div>
    <div class="box-body">
        <span class="label label-success">Bontoala</span>
        <span class="label label-success">Biringkanaya</span>
        <span class="label label-success">Panakukkang</span>
        <span class="label label-success">Makassar</span>
        <span class="label label-success">Mamajang</span>
        <span class="label label-success">Manggala</span>
        <span class="label label-success">Mariso</span>
        <span class="label label-success">Rappocini</span>
        <span class="label label-success">Tallo</span>
        <span class="label label-success">Tamalanrea</span>
        <span class="label label-success">Tamalate</span>
        <span class="label label-success">Ujung Pandang</span>
        <span class="label label-success">Ujung Tanah</span>
        <span class="label label-success">Wajo</span>


    </div>
</div>
<div class="box box-danger">
    <div class="box-header">
        <h4><i class="fa fa-chevron-right small"></i>  Link terkait</h4>
    </div>
    <div class="box-body">
        <ul class="list-img list-slide">
            <li>
                <a href="http://indonesia.go.id/" target="_blank"><img src="dist/img/image/link-terkait/1.jpg"></a>
            </li>
            <li>
                <a href="http://musrenbang.makassar.go.id/" target="_blank"><img src="dist/img/image/link-terkait/2.jpg"></a>
            </li>
            <li>
                <a href="http://www.lpse-makassar.info/eproc/" target="_blank"><img src="dist/img/image/link-terkait/3.jpg"></a>
            </li>
            <li>
                <a href="http://bappenas.go.id" target="_blank"><img src="dist/img/image/link-terkait/4.jpg"></a>
            </li>
            <li>
                <a href="http://makassarkota.go.id" target="_blank"><img src="dist/img/image/link-terkait/5.jpg"></a>
            </li>
        </ul>
    </div>
</div>