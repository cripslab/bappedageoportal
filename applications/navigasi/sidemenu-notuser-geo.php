<div class="box box-geo-side">

    <div class="box-user">
        <div class="callout callout-danger">
            <h4><i class="fa fa-warning"></i> Anda Belum Login!</h4>
            <p>Silahkan login terlebih dahulu untuk membuat laporan, atau daftar jika anda belum pernah mendaftar di website Geoportal Makassar.</p>
        </div>
    </div>

    <div class="box-input-report">
        <div class="header-geo-side bg-red">
            <h4><i class="fa fa-edit "></i> Buat laporan</h4>
        </div>
        <form class="form-horizontal" enctype="multipart/form-data">
            <div class="form-group">
                <label for="" class="col-sm-1 control-label"><i class="fa fa-bullhorn text-muted"></i></label>
                <div class="col-sm-11">
                    <input type="text" class="form-control" id="" placeholder="Judul Laporan" disabled>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-1 control-label"><i class="fa fa-check-square-o text-muted"></i></label>
                <div class="col-sm-11">
                    <textarea class="form-control" id="" placeholder="Isi laporan?" disabled></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-1 control-label"><i class="fa fa-map-pin text-muted"></i></label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="" placeholder="Latitude" disabled>
                </div>
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="" placeholder="Longtitude" disabled>
                </div>
                <!--<div class="col-sm-11">
                    <a href="#collapseTwo" data-parent="#accordion" data-toggle="collapse" aria-expanded="true" class="pull-right" data-original-title="peta" disabled>Tandai di <i class="fa fa-map" disabled></i></a>
                </div>-->
                <div class="col-md-12">
                    <div id="accordion">
                        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                        <div class="panel">
                            <div class="panel-collapse collapse" id="collapseTwo" aria-expanded="true" style="">
                                <div class="">
                                    <iframe width="100%" height="250" frameborder="0" src="https://mapsengine.google.com/map/u/0/embed?mid=z8IhMi3HVN6o.k3-sVaD4eX_o" marginwidth="0" marginheight="0" scrolling="no"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-1 control-label"><i class="fa fa-image text-muted"></i></label>
                <div class="col-sm-3">
                    <input id="file-3" type="file" multiple="true" disabled>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-1 control-label"><i class="fa fa-list text-muted"></i></label>
                <div class="col-sm-6">
                    <select class="form-control" id="" placeholder="Jenis laporan" disabled>
                        <option>Pemerintah</option>
                        <option>Jalan</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6">
                    <a class="btn btn-default btn-block" disabled>Kirim laporan</a>
                </div>
            </div>
        </form>
    </div>
</div>