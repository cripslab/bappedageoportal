<div class="box box-geo-side">

    <div class="box-user">
        <div class="info-user">
            <div class="avatar">
                <img src="dist/img/theme/avatar.jpg" />
            </div>
            <div class="status-user">
                <p class="name-user">Wandy Rifaldi</p>
                <p>Makassar</p>
                <p>Last login :<span>10 day ago</span>
                </p>
            </div>
        </div>

        <ul class="list-respon">
            <div class="row">
                <div class="col-md-6">
                    <li class="item-list">
                        <span class="title">Laporan</span>
                        <span class="value">10</span>
                    </li>
                </div>
                <div class="col-md-6">
                    <li class="item-list">
                        <span class="title">Tanggapan</span>
                        <span class="value">5</span>
                    </li>
                </div>
            </div>
        </ul>

        <ul class="list-nav-user">
            <li><a href=""><span class="glyphicon glyphicon-envelope"></span> Notifikasi <span class="badge pull-right">42</span></a>
            </li>
            <li><a href="index.php?p=geo-profil-user"><span class="glyphicon glyphicon-user"></span> Profil</a>
            </li>
            <li><a href=""><span class="glyphicon glyphicon-off"></span> Keluar</a>
            </li>
        </ul>
    </div>

    <div class="box-input-report">
        <div class="header-geo-side bg-aqua">
            <h4><i class="fa fa-edit "></i> Buat laporan</h4>
        </div>
        <form class="form-horizontal" enctype="multipart/form-data">
            <div class="form-group">
                <label for="" class="col-sm-1 control-label"><i class="fa fa-bullhorn text-green"></i></label>
                <div class="col-sm-11">
                    <input type="text" class="form-control" id="" placeholder="Judul Laporan">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-1 control-label"><i class="fa fa-check-square-o text-aqua"></i></label>
                <div class="col-sm-11">
                    <textarea class="form-control" id="" placeholder="Isi laporan?"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-1 control-label"><i class="fa fa-map-pin text-green"></i></label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="" placeholder="Latitude">
                </div>
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="" placeholder="Longtitude">
                </div>
                <div class="col-sm-11">
                    <a href="#collapseTwo" data-parent="#accordion" data-toggle="collapse" aria-expanded="true" class="pull-right" data-original-title="peta">Tandai di <i class="fa fa-map"></i></a>
                </div>
                <div class="col-md-12">
                    <div id="accordion">
                        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                        <div class="panel">
                            <div class="panel-collapse collapse" id="collapseTwo" aria-expanded="true" style="">
                                <div class="">
                                    <iframe width="100%" height="250" frameborder="0" src="https://mapsengine.google.com/map/u/0/embed?mid=z8IhMi3HVN6o.k3-sVaD4eX_o" marginwidth="0" marginheight="0" scrolling="no"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-1 control-label"><i class="fa fa-image text-aqua"></i></label>
                <div class="col-sm-3">
                    <input id="file-3" type="file" multiple=true>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="col-sm-1 control-label"><i class="fa fa-list text-green"></i></label>
                <div class="col-sm-6">
                    <select class="form-control" id="" placeholder="Jenis laporan">
                        <option>Pemerintah</option>
                        <option>Jalan</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6">
                    <a class="btn btn-success btn-block">Kirim laporan</a>
                </div>
            </div>
        </form>
    </div>
</div>