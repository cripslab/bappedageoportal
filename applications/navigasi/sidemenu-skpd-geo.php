<div class="box box-geo-side">

    <div class="box-user">
        <div class="info-user">
            <div class="avatar">
                <img src="dist/img/theme/avatar.jpg" />
            </div>
            <div class="status-user">
                <p class="name-user">Dinas Pekerjaan Umum</p>
                <p>Makassar</p>
                <p>Last login :<span>10 day ago</span>
                </p>
            </div>
        </div>

        <ul class="list-respon">
            <div class="row">
                <div class="col-md-6">
                    <li class="item-list">
                        <span class="title">Disposisi</span>
                        <span class="value">10</span>
                    </li>
                </div>
                <div class="col-md-6">
                    <li class="item-list">
                        <span class="title">Menanggapi</span>
                        <span class="value">5</span>
                    </li>
                </div>
            </div>
        </ul>

        <ul class="list-nav-user">
            <li><a href=""><span class="glyphicon glyphicon-envelope"></span> Notifikasi <span class="badge pull-right">42</span></a>
            </li>
            <li><a href="index.php?p=geo-profil-user"><span class="glyphicon glyphicon-user"></span> Profil</a>
            </li>
            <li><a href=""><span class="glyphicon glyphicon-off"></span> Keluar</a>
            </li>
        </ul>
    </div>

    <div class="box-input-report">
        <div class="header-geo-side bg-orange">
            <h4><i class="fa fa-envelope-o"></i> Notifikasi</h4>
        </div>
        <div class="box-list-notif">
                                        <ul class="notif-list">
                                            <li>
                                                <div class="notif-image">
                                                    <img src="dist/img/theme/avatar.jpg" />
                                                </div>
                                                <div class="notif-text">
                                                    <p><a href="">Dinas Pekerjaan Umum</a> menanggapi kembali laporan <a href="">#1092834</a> yang telah anda tanggapi oleh <a href="">wandy rifaldi</a></p>
                                                    <span class="date sub-text">Senin, 12 agustus 2014</span>

                                                </div>
                                            </li>
                                            <li>
                                                <div class="notif-image">
                                                    <img src="dist/img/theme/avatar.jpg" />
                                                </div>
                                                <div class="notif-text">
                                                    <p><a href="">Dinas Pekerjaan Umum</a> menanggapi laporan <a href="">#1092834</a> yang di laporkan oleh <a href="">wandy rifaldi</a></p>
                                                    <span class="date sub-text">Senin, 12 agustus 2014</span>

                                                </div>
                                            </li>
                                            <li>
                                                <div class="notif-image">
                                                    <img src="dist/img/theme/avatar.jpg" />
                                                </div>
                                                <div class="notif-text">
                                                    <p><a href="">Pemerintah Kota</a> mendisposisikan laporan <a href="">#1092834</a> ke Dinas Pekerjaan Umum</p>
                                                    <span class="date sub-text">Senin, 12 agustus 2014</span>

                                                </div>
                                            </li>
                                        </ul>
                                    </div>
        
    </div>
</div>