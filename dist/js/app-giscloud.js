$(document).ready(function() {
var mapId = '454305',
    $ = giscloud.exposeJQuery(),
    viewer, toolbar, layerList;

giscloud.apiKey("71bf5052ba6c4bbf378a9b93dbc0717f");

giscloud.ready(function () {

    //show map
    viewer = new giscloud.Viewer("mapViewer", mapId);
    
    //add toolbar 
    toolbar = new giscloud.ui.Toolbar({
        viewer: viewer,
        container: "toolbar",
        defaultTools: ["pan", "zoom", "full", "measure", "azimuth", "areaSelect", "info"],
    });
    
    // create the toolbar_2 and add the tools
    toolbar_2 = new giscloud.ui.Toolbar({
        viewer: viewer,
        container: "toolbar_2",
    }).add(

    // point tool
    new giscloud.ui.Toolbar.Tool("pointTool", {
        instant: true,
        styles: {
            caption: "Point Tool",
            showCaption: false,
            showTooltips: true,
            cssClass: "ic-gis fa fa-map-marker",
            active: "",
            hover: "ic-gis-hover"
        },
        actions: {
            activation: function (viewer) {

                // start drawing
                viewer.graphic.draw("point",

                function (feat) {

                    // output wkt
                    outputOgcWkt(feat.geometry().toOGC());

                });
            }
        }
    }),

    // line tool
    new giscloud.ui.Toolbar.Tool("lineTool", {
        instant: true,
        styles: {
            caption: "Line Tool",
            showCaption: false,
            showTooltips: true,
            cssClass: "ic-gis fa fa-expand",
            active: "",
            hover: "ic-gis-hover"
        },
        actions: {
            activation: function (viewer) {

                // start drawing
                viewer.graphic.draw("line",

                function (feat) {

                    // output wkt
                    outputOgcWkt(feat.geometry().toOGC());

                });
            }
        }
    }),

    // polygon tool
    new giscloud.ui.Toolbar.Tool("polygonTool", {
        instant: true,
        styles: {
            caption: "Polygon Tool",
            showCaption: false,
            showTooltips: true,
            cssClass: "ic-gis fa fa-object-group",
            active: "",
            hover: "ic-gis-hover"
        },
        actions: {
            activation: function (viewer) {

                // start drawing
                viewer.graphic.draw("polygon",

                function (feat) {

                    // output wkt
                    outputOgcWkt(feat.geometry().toOGC());

                });
            }
        }
    }),

    // clear
    new giscloud.ui.Toolbar.Tool("clearTool", {
        instant: true,
        styles: {
            caption: "Clear Tool",
            showCaption: false,
            showTooltips: true,
            cssClass: "ic-gis fa fa-eraser",
            active: "",
            hover: "ic-gis-hover"
        },
        actions: {
            activation: function (viewer) {
                // clear graphic layer
                viewer.graphic.clear();
            }
        }
    }));
    
    //add layer list
    layerList = new giscloud.ui.LayerList(viewer, "layerList", true);
    
    //filter based on selected layer 
    layerList.bind("dblclick", layerListIsSelected);  
    
    //zoom to feature and get feature info on feature double click
    viewer.featureDoubleclick(featureClick);

});
});