$(document).ready(function () {

    $("#owl-slide-full").owlCarousel({
        navigation: false, // Show next and prev buttons
        pagination: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        autoPlay: true
    });
    $('ul.list-slide').carouFredSel({
        direction: "up",
        width: 300,
        items: 3,
        scroll: {
            items: 1,
        }
    });
    $('#lisfit').carouFredSel({
        direction: "left",
        width: '100%',
        height: 90,
        items: 4,
        scroll: {
            items: 1,
        }
    });
    $('#lisfit-geo').carouFredSel({
        direction: "left",
        width: '100%',
        height: 70,
        items: 5,
        scroll: {
            items: 1,
        }
    });
    $('#img-lapor').carouFredSel({
        items: 1,
        direction: "up",
        auto: {
            easing: "elastic",
            duration: 1000,
            timeoutDuration: 2000,
            pauseOnHover: true
        },
        prev: {
            button: "#foo2_prev",
            key: "left"
        },
        next: {
            button: "#foo2_next",
            key: "right"
        },
    });
    $('#data-table').dataTable({});

});

$(document).ready(function () {
    $.asm = {};
    $.asm.panels = 1;

    function sidebar(panels) {
        $.asm.panels = panels;
        if (panels === 1) {
            $('#content-map').removeClass('col-md-9');
            $('#content-map').addClass('col-md-12 no-sidebar');
            $('#sidebar-layer').hide();
        } else if (panels === 2) {
            $('#content-map').removeClass('col-md-12 no-sidebar');
            $('#content-map').addClass('col-md-9');
            $('#sidebar-layer').show();
            $('#sidebar-layer').height($(window).height() - 50);
            return google.maps.event.trigger($.asm.theMap, 'resize');
        }
    };

    $('#toggleSidebar').click(function () {
        if ($.asm.panels === 1) {
            $('#toggleSidebar i').addClass('icon-chevron-left');
            $('#toggleSidebar i').removeClass('icon-chevron-right');
            return sidebar(2);
        } else {
            $('#toggleSidebar i').removeClass('icon-chevron-left');
            $('#toggleSidebar i').addClass('icon-chevron-right');
            return sidebar(1);
        }
    });

});