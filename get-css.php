<!-- Bootstrap Master CSS -->
<link href="dist/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Default Master CSS -->
<link href="dist/css/style.css" rel="stylesheet">
<link href="dist/css/crips.min.css" rel="stylesheet">
<link href="dist/css/skins/_all-skins.min.css" rel="stylesheet">

<!-- All Plugins CSS -->
<link href="plugins/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet">
<link href="plugins/ionicons-2.0.1/css/ionicons.min.css" rel="stylesheet">
<link href="plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="plugins/owl-carousel/owl.theme.css" rel="stylesheet">
<link href="plugins/datepicker/datepicker3.css" rel="stylesheet">
<link href="plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="plugins/select2/select2.min.css" rel="stylesheet">
<link href="plugins/datatables/dataTables.bootstrap.css" rel="stylesheet">